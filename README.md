# 🚀 Laravel 11 & PHP 8.2 Event Sourcing Project

## 👨‍💻 About the Developer: David Vanmak
Welcome to my exploration of Event Sourcing in Laravel 11, utilizing PHP 8.2. As a seasoned PHP Developer with a passion for Laravel, this project serves as a demonstration of my capabilities in implementing complex architectural patterns, specifically focusing on Event Sourcing using Spatie's Event Sourcing package.

## 🚀 Project Highlights
- **Event Sourcing with Spatie**: Leveraging Spatie's Event Sourcing package for Laravel to implement event-driven architecture.
- **Latest Tech Stack**: Developed with Laravel 11 & PHP 8.2, embracing the newest features and improvements.
- ~~**Focus on Clean Architecture & CQRS**: Preparing the ground for scalable and maintainable design through Clean Architecture and Command Query Responsibility Segregation (CQRS) principles.~~
- **Developer-Centric Design**: Crafted to demonstrate advanced Laravel capabilities and best practices in software design.

## 🛠️ Skills in Action
- **PHP 8.2 Features**: Utilizing the latest PHP enhancements for secure and efficient coding practices.
- **Laravel 11 Expertise**: Showcasing the power and flexibility of Laravel 11 for modern web application development.
- **Code Quality**: Adhering to SOLID principles, PSR standards, and embracing clean code practices.

## 📚 Quick Setup
1. **Clone the Repository**: `git clone git@gitlab.com:peuks/app.git`
2. **Install Dependencies**: Run `composer install` to install the required PHP packages.
3. **Environment Configuration**: Copy `.env.example` to `.env` and configure your environment settings.
4. **Generate Application Key**: `php artisan key:generate` to set your application key.
5. **Run Migrations**: `php artisan migrate` to set up your database schema.
6. **Start the Application**: Use `php artisan serve` to launch the application.

## 📐 Architectural Overview
- **Domain Layer**: ~~Core logic encapsulating the event sourcing mechanisms for entities like posts and comments.~~
- **Application Layer**: ~~Orchestrates the flow of data to and from the domain layer, handling user commands and queries.~~
- **Infrastructure Layer**: ~~External services and integrations, including database and event store configurations.~~
- **Presentation Layer**: ~~User interface considerations, focusing on delivering a seamless experience.~~

## 🌟 Why This Project Stands Out
- **Innovative Approach**: Demonstrates the practical application of event sourcing in a Laravel project.
- **Architectural Clarity**: Provides a clear example of implementing Clean Architecture and CQRS in Laravel.
- **Modern Development Practices**: Showcases the use of the latest Laravel and PHP features, emphasizing code quality and maintainability.

## 🛡️ Future Plans: Testing & Security
- **Comprehensive Testing**: Plans to develop a thorough testing suite to ensure reliability and performance.
- **Security Practices**: Aiming to incorporate security best practices, prioritizing the protection of user data.

## 📢 Let's Connect
I'm always open to discussing this project, potential collaborations, or sharing insights on Laravel and PHP development. Connect with me on [LinkedIn](https://www.linkedin.com/in/davidvanmak/), and let's make something great together!
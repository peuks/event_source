<?php

namespace App\Providers;

use App\Contracts\DateProvider;
use App\Services\CarboneDateProvider;
use Illuminate\Support\ServiceProvider;

class DateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(DateProvider::class, CarboneDateProvider::class);

    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}

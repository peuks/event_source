<?php

namespace App\Contracts;

interface DateProvider
{
    public function currentDateTime(): \DateTimeInterface;
}

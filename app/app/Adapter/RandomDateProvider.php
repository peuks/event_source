<?

namespace App\Services;

use App\Contracts\DateProvider;
use Carbon\Carbon;

class RandomDateProvider implements DateProvider
{
    /**
     * Returns a random date between now and three months ago.
     *
     * @return \DateTimeInterface
     */
    public function currentDateTime(): \DateTimeInterface
    {
        return Carbon::today()->subMonths(rand(0, 3));
    }
}

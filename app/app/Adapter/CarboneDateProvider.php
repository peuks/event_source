<?php
namespace App\Services;

use App\Contracts\DateProvider;

class CarboneDateProvider implements DateProvider
{
    public function currentDateTime(): \DateTimeInterface
    {
        return new \DateTimeImmutable();
    }
}

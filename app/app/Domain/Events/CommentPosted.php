<?
namespace App\Domain\Events;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class CommentPosted extends ShouldBeStored
{
    public function __construct(
        public string $id,
        public string $publicationId,
        public string $userId,
        public string $content,
        public \DateTimeInterface $createdAt


    ) {
    }
}

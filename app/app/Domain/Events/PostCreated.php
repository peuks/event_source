<?
namespace App\Domain\Events;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class PostCreated extends ShouldBeStored
{
    public function __construct(
        public string $postId,
        public string $userId,
        public string $title,
        public string $content,
        public \DateTimeInterface $createdAt
    ) {
    }
}

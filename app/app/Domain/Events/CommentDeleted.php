<?
namespace App\Domain\Events;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class CommentDeleted extends ShouldBeStored
{
    public function __construct(
        public string $commentId,
        public \DateTimeInterface $deletedAt

    ) {
    }
}
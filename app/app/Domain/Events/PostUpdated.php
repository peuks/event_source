<?
namespace App\Domain\Events;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class PostUpdated extends ShouldBeStored
{
    public function __construct(
        public string $postId,
        public string $title,
        public string $content,
        public \DateTimeInterface $updatedAt

    ) {
    }
}

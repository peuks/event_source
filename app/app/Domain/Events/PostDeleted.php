<?
namespace App\Domain\Events;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class PostDeleted extends ShouldBeStored
{
    public function __construct(
        public string $postId,
        public \DateTimeInterface $deletedAt
    ) {
    }
}

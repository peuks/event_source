<?
namespace App\Domain\Events;

use Spatie\EventSourcing\StoredEvents\ShouldBeStored;

class CommentUpdated extends ShouldBeStored
{
    public function __construct(
        public string $commentId,
        public string $content,
        public \DateTimeInterface $updatedAt

    ) {
    }
}
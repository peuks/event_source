<?php

namespace App\Domain\Reactors;

use Spatie\EventSourcing\EventHandlers\Reactors\Reactor;
use App\Domain\Events\PostCreated;
use Illuminate\Support\Facades\Log;

class PostLogReactor extends Reactor
{
    public function onPostCreated(PostCreated $event)
    {
        $message = "A new post was created by user ID {$event->userId} with title: '{$event->title}'";
        Log::info("Hem hem ...$message");
    }
}

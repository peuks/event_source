<?
namespace App\Domain\Aggregates;

use App\Domain\Events\CommentDeleted;
use App\Domain\Events\CommentPosted;
use App\Domain\Events\CommentUpdated;
use Spatie\EventSourcing\AggregateRoots\AggregateRoot;

class CommentAggregate extends AggregateRoot
{
    public function __construct()
    {
    }
    public function postComment(
        string $postId,
        string $publicationId,
        string $userId,
        string $content,
        \DateTimeInterface $createdAt
    ): self {
        $this->recordThat(
            new CommentPosted(
                id: $postId,
                publicationId: $publicationId,
                userId: $userId,
                content: $content,
                createdAt: $createdAt
            )
        );

        return $this;
    }

    public function updateComment(
        string $commentId,
        string $content,
        \DateTimeInterface $updatedAt

    ): self {
        $this->recordThat(
            new CommentUpdated(
                commentId: $commentId,
                content: $content,
                updatedAt: $updatedAt
            )
        );
        return $this;
    }

    public function deleteComment(
        string $commentId,
        \DateTimeInterface $deletedAt
    ): self {
        $this->recordThat(
            new CommentDeleted(
                commentId: $commentId,
                deletedAt: $deletedAt
            )
        );
        return $this;
    }
}
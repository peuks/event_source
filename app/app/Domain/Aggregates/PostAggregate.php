<?
namespace App\Domain\Aggregates;

use App\Domain\Events\PostCreated;
use App\Domain\Events\PostDeleted;
use App\Domain\Events\PostUpdated;
use Spatie\EventSourcing\AggregateRoots\AggregateRoot;

class PostAggregate extends AggregateRoot
{
    public static function snapshotEvery(): int
    {
        return 100;
    }
    public function createPost(
        string $postId,
        string $userId,
        string $title,
        string $content,
        \DateTimeInterface $createdAt
    ) {
        $this->recordThat(
            new PostCreated(
                postId: $postId,
                userId: $userId,
                title: $title,
                content: $content,
                createdAt: $createdAt
            )
        );
        return $this;
    }

    public function updatePost(
        string $postId,
        string $title,
        string $content,
        \DateTimeInterface $updatedAt
    ) {
        $this->recordThat(
            new PostUpdated(
                postId: $postId,
                title: $title,
                content: $content,
                updatedAt: $updatedAt
            )
        );
        return $this;
    }

    public function deletePost(string $postId, \DateTimeInterface $delatedAt)
    {
        $this->recordThat(
            new PostDeleted(
                postId: $postId,
                deletedAt: $delatedAt
            )
        );
        return $this;
    }

}

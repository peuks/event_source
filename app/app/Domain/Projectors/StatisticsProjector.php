<?php
namespace App\Domain\Projectors;

use Carbon\Carbon;
use App\Models\MonthlyStatistic;
use App\Domain\Events\PostCreated;
use Illuminate\Support\Facades\DB;
use App\Domain\Events\CommentPosted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\EventSourcing\EventHandlers\Projectors\Projector;

class StatisticsProjector extends Projector implements ShouldQueue
{
    public int $weight = 3;
    public function onPostCreated(PostCreated $event)
    {
        $currentMonth = Carbon::now()->format('Y-m');
        $lastMonth = Carbon::now()->subMonth()->format('Y-m');

        $lastMonthTotalPosts = MonthlyStatistic::where('month_year', $lastMonth)->value('total_posts');

        MonthlyStatistic::updateOrCreate(
            ['month_year' => $currentMonth],
            ['total_posts' => \DB::raw('total_posts + 1')]
        )->when($lastMonthTotalPosts, function ($query) use ($lastMonthTotalPosts) {
            $postsDelta = $lastMonthTotalPosts > 0 ?
                (($query->total_posts - $lastMonthTotalPosts) / $lastMonthTotalPosts) * 100 : 100;
            $query->posts_delta = $postsDelta;
        }, fn($query) => $query->posts_delta = 100);
    }


    public function onCommentPosted(CommentPosted $event)
    {
        $currentMonth = Carbon::now()->format('Y-m');
        $lastMonth = Carbon::now()->subMonth()->format('Y-m');

        $lastMonthTotalComments = MonthlyStatistic::where('month_year', $lastMonth)->value('total_comments');

        $statistic = MonthlyStatistic::updateOrCreate(
            ['month_year' => $currentMonth],
            ['total_comments' => DB::raw('total_comments + 1')]
        );

        $statistic->refresh();

        $recentComments = json_decode($statistic->recent_comments ?? '[]');
        $newComment = [
            'id' => $event->id,
            'publicationId' => $event->publicationId,
            'userId' => $event->userId,
            'content' => $event->content,
            'createdAt' => $event->createdAt->format('Y-m-d H:i:s'),
        ];

        array_unshift($recentComments, $newComment);
        $recentComments = array_slice($recentComments, 0, 5);

        // Sauvegarder les changements
        $statistic->recent_comments = json_encode($recentComments);
        $statistic->comments_delta = ($lastMonthTotalComments > 0) ? (($statistic->total_comments - $lastMonthTotalComments) / $lastMonthTotalComments) * 100 : 100;
        $statistic->save();
    }



}

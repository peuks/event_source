<?php

namespace App\Domain\Projectors;

use App\Models\Comment;
use App\Domain\Events\CommentPosted;
use App\Domain\Events\CommentDeleted;
use App\Domain\Events\CommentUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\EventSourcing\EventHandlers\Projectors\Projector;

class CommentProjector extends Projector implements ShouldQueue
{
    public int $weight = 2;
    public function onCommentPosted(CommentPosted $event)
    {

        if (!Comment::find($event->id))
            Comment::create([
                'id' => $event->id,
                'content' => $event->content,
                'publication_id' => $event->publicationId,
                'user_id' => $event->userId,
            ]);
    }

    public function onCommentUpdated(CommentUpdated $event)
    {
        $comment = Comment::find($event->commentId);
        if ($comment)
            $comment->update([
                'content' => $event->content,
            ]);
    }

    public function onCommentDeleted(CommentDeleted $event)
    {
        Comment::where('id', $event->commentId)->delete();
    }
}

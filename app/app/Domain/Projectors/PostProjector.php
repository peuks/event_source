<?
namespace App\Domain\Projectors;

use App\Models\Publication;
use App\Domain\Events\PostCreated;
use App\Domain\Events\PostDeleted;
use App\Domain\Events\PostUpdated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\EventSourcing\EventHandlers\Projectors\Projector;

class PostProjector extends Projector implements ShouldQueue
{
    public int $weight = 1;
    public function onPostCreated(PostCreated $event)
    {
        if (!Publication::find($event->postId))
            Publication::create([
                'id' => $event->postId,
                'user_id' => $event->userId,
                'title' => $event->title,
                'content' => $event->content,
                'created_at' => $event->createdAt
            ]);
    }

    public function onPostUpdated(PostUpdated $event)
    {
        Publication::update([
            'id' => $event->postId,
            'title' => $event->title,
            'content' => $event->content,
            'updated_at' => $event->updatedAt
        ]);
    }

    public function onPostDeleted(PostDeleted $event)
    {
        Publication::where('id', $event->postId)->delete();
    }
}

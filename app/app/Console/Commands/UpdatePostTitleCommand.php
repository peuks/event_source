<?php

namespace App\Console\Commands;

use App\Domain\Aggregates\PostAggregate;
use Illuminate\Console\Command;

class UpdatePostTitleCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:update-title {postId} {newTitle} {newcontent}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the title of an existing post';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $postId = $this->argument('postId');
        $newTitle = $this->argument('newTitle');
        $content = $this->argument('newcontent');

        PostAggregate::retrieve($postId)
            ->updatePost(
                postId: $postId,
                title: $newTitle,
                content: $content,
            )
            ->persist();

        $this->info("Post with ID $postId updated successfully.");
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Support\Str;
use Illuminate\Console\Command;
use App\Domain\Aggregates\CommentAggregate;

class PostCommentCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'comment:post {postId} {userId} {content}';


    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Posts a new comment to a publication';


    /**
     * Execute the console command.
     */
    public function handle()
    {
        $postId = $this->argument('postId');
        $userId = $this->argument('userId');
        $content = $this->argument('content');

        $commentId = (string) Str::uuid();

        CommentAggregate::retrieve($commentId)
            ->postComment(
                postId: $postId,
                publicationId: $postId,
                userId: $userId,
                content: $content
            )
            ->persist();

        $this->info("Comment with ID $commentId posted successfully to post $postId.");
    }
}

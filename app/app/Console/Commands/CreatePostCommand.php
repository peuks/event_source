<?php

namespace App\Console\Commands;

use App\Domain\Aggregates\PostAggregate;
use App\Models\User;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class CreatePostCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'post:create{title} {content}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a new post';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $userId = User::first()->id;
        $title = $this->argument('title');
        $content = $this->argument('content');

        $postId = (string) Str::uuid();

        PostAggregate::retrieve($postId)
            ->createPost(
                postId: $postId,
                userId: $userId,
                title: $title,
                content: $content
            )
            ->persist();

        $this->info("Post with ID $postId created successfully.");
    }
}

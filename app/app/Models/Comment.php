<?php

namespace App\Models;

use App\Traits\UUID;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory, UUID;
    public $timestamps = false;
    protected $fillable = [
        'id',
        'content',
        'publication_id',
        'user_id',
        'created_at',
        'updated_at'
    ];

    public function publication()
    {
        return $this->belongsTo(Publication::class);
    }

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}

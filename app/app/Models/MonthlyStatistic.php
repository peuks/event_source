<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MonthlyStatistic extends Model
{
    use HasFactory;
    protected $fillable = [
        'month_year',
        'total_posts',
        'posts_delta',
        'total_comments',
        'comments_delta',
        'recent_comments',
        'top_contributors'
    ];

    public function setMonthYear($monthYear): self
    {
        $this->month_year = $monthYear;
        return $this;
    }

    public function setTotalPosts($totalPosts): self
    {
        $this->total_posts = $totalPosts;
        return $this;
    }

    public function setPostsDelta($postsDelta): self
    {
        $this->posts_delta = $postsDelta;
        return $this;
    }

    public function setTotalComments($totalComments): self
    {
        $this->total_comments = $totalComments;
        return $this;
    }

    public function setCommentsDelta($commentsDelta): self
    {
        $this->comments_delta = $commentsDelta;
        return $this;
    }

    public function setRecentComments(array $comments): self
    {
        $this->recent_comments = json_encode($comments);
        return $this;
    }

    public function setTopContributors(array $contributors): self
    {
        $this->top_contributors = json_encode($contributors);
        return $this;
    }
}

<?php
namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class NotificationsController extends Controller
{
    public function __invoke(Request $request): Response
    {
        return Inertia::render(
            component: 'Setting/Notifications',
            props: []
        );
    }
}

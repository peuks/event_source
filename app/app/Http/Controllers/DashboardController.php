<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Inertia\Response;

class DashboardController extends Controller
{
    /**
     * Handle the incoming request.
     */
    public function __invoke(Request $request): Response
    {
        // Total posts for current and previous month
        $totalPostsCurrentMonth = Comment::whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', Carbon::now()->month)
            ->count();

        $totalPostsPreviousMonth = Comment::whereYear('created_at', Carbon::now()->subMonth()->year)
            ->whereMonth('created_at', Carbon::now()->subMonth()->month)
            ->count();

        // Calculate the percentage change in posts
        $postsDelta = $totalPostsPreviousMonth > 0 ?
            (($totalPostsCurrentMonth - $totalPostsPreviousMonth) / $totalPostsPreviousMonth) * 100 : 100;

        // Total comments for current and previous month (assumption based on the provided code snippet)
        $totalCommentsCurrentMonth = Comment::whereYear('created_at', Carbon::now()->year)
            ->whereMonth('created_at', Carbon::now()->month)
            ->count(); // Assuming comments are counted like posts

        $totalCommentsPreviousMonth = Comment::whereYear('created_at', Carbon::now()->subMonth()->year)
            ->whereMonth('created_at', Carbon::now()->subMonth()->month)
            ->count();

        // Calculate the percentage change in comments
        $commentsDelta = $totalCommentsPreviousMonth > 0 ?
            (($totalCommentsCurrentMonth - $totalCommentsPreviousMonth) / $totalCommentsPreviousMonth) * 100 : 100;

        return Inertia::render('Dashboard', [
            'recentComments' => Comment::with('author')
                ->latest()
                ->take(5)
                ->get()
                ->map(
                    fn(Comment $comment) => [
                        'id' => $comment->id,
                        'content' => $comment->content,
                        'created_at' => $comment->created_at->toDateTimeString(),
                        'name' => $comment->author->name,
                        'email' => $comment->author->email,
                    ]
                ),
            'topContributors' => User::withCount('comments as comments_count')
                ->orderByDesc('comments_count')
                ->take(5)
                ->get()
                ->map(
                    fn(User $user) => [
                        'name' => $user->name,
                        'email' => $user->email,
                        'comments_count' => $user->comments_count,
                    ]
                ),
            'totalPostsCurrentMonth' => $totalPostsCurrentMonth,
            'postsDelta' => $postsDelta,
            'totalCommentsCurrentMonth' => $totalCommentsCurrentMonth,
            'commentsDelta' => $commentsDelta,
        ]);
    }
}

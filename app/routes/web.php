<?php

use App\Http\Controllers\CommentsController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\Setting\AccountController;
use App\Http\Controllers\Setting\AppearanceController;
use App\Http\Controllers\Setting\DisplayController;
use App\Http\Controllers\Setting\NotificationsController;
use App\Http\Controllers\SettingsController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::get('/dashboard', DashboardController::class)->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware(['auth', 'verified'])->group(function () {
    Route::get('/', DashboardController::class)->name('dashboard');
    Route::get('/posts', PostsController::class)->name('posts');
    Route::get('/comments', CommentsController::class)->name('comments');
});




Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
    Route::get('/settings', SettingsController::class)->name('settings');
    Route::get('/settings/account', AccountController::class)->name('settings.account');
    Route::get('/settings/appearance', AppearanceController::class)->name('settings.appearance');
    Route::get('/settings/notifications', NotificationsController::class)->name('settings.notifications');
    Route::get('/settings/display', DisplayController::class)->name('settings.display');
});

require __DIR__ . '/auth.php';

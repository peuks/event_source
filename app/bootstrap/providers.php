<?php

return [
    App\Providers\AppServiceProvider::class,
    App\Providers\DateServiceProvider::class,
];

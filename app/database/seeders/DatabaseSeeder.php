<?php

use App\Models\User;
use App\Services\RandomDateProvider;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use App\Domain\Aggregates\PostAggregate;
use App\Domain\Aggregates\CommentAggregate;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();
        $dateProvider = new RandomDateProvider();

        for ($u = 0; $u < 10; $u++) {
            $user = User::create([
                'id' => Str::uuid(),
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'password' => bcrypt('password'),
            ]);

            for ($i = 0; $i < 10; $i++) {
                $postId = (string) Str::uuid();
                $title = $faker->sentence;
                $content = $faker->paragraph;
                PostAggregate::retrieve($postId)
                    ->createPost(
                        postId: $postId,
                        userId: $user->id,
                        title: $title,
                        content: $content,
                        createdAt: Carbon::now()->subYearS(2)
                    )
                    ->persist();

                $commentsCount = rand(1, 5);
                for ($j = 0; $j < $commentsCount; $j++) {
                    $commentId = (string) Str::uuid();
                    $commentContent = $faker->sentence;

                    CommentAggregate::retrieve($commentId)
                        ->postComment(
                            postId: $commentId,
                            publicationId: $postId,
                            userId: $user->id,
                            content: $commentContent,
                            createdAt: $dateProvider->currentDateTime()
                        )
                        ->persist();
                }
            }
        }
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('monthly_statistics', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->string('month_year', 7);
            $table->integer('total_posts')->default(0);
            $table->decimal('posts_delta', 5, 2)->default(0);
            $table->integer('total_comments')->default(0);
            $table->decimal('comments_delta', 5, 2)->default(0);
            $table->json('recent_comments')->default(json_encode([]));
            $table->json('top_contributors')->default(json_encode([]));
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('monthly_statistics');
    }
};

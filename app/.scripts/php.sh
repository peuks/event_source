#!/bin/bash
# Runs PHP CLI commands within the PHP container
docker compose -f $PWD/docker-compose.yml exec app php $@

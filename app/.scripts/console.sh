#!/bin/bash
# Executes Laravel Artisan commands within the PHP container
docker compose -f $PWD/docker-compose.yml exec app php artisan $@

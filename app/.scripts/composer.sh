#!/bin/bash
# Runs Composer commands within the PHP container
docker compose -f $PWD/docker-compose.yml exec app composer $@

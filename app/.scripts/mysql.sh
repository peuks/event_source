#!/bin/bash
# Executes MySQL commands within the DB container
docker compose -f $PWD/docker-compose.yml exec db mysql $@
